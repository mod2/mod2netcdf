
# coding: utf-8

# # FloPy
# 
# ### Demo of netCDF and shapefile export capabilities within the flopy export module. 

## This program assumes/creates the following directory structure except
##  where provided arguments deviating from it:
##    - in the same directory as the MODLOW model's name-file exists a folder
##       named "grid" which contains a shapefile corresponding to the model



## TODO
##    - Implement argument parsing, specifically:
##       - required input file & at least one required output file
##       - checks for existence of expected files unless checks irrelevant
##          by arguments
##       - cf argparse, https://docs.python.org/3.3/library/argparse.html
##       - cf os.path, https://docs.python.org/2/library/os.path.html
##    - Reoganise entirety below:
##       - moving inapplicable comments to separate file for reference
##       - resectioning along programmatic lines, rather than Jupyter's
##       - separating into main and subroutines:
##          - differing subroutines especially needed for different exports,
##             the script itself should be written to select the desired export
##    - Defer reliance upon PRJ file to shapefile, if possible


# In[1]:


## Libraries of context
from __future__ import print_function
import os
import sys

# run installed version of flopy or add local path
try:
    import flopy
except:
    fpth = os.path.abspath(os.path.join('..', '..'))
    sys.path.append(fpth)
    import flopy

## Geospatial libraries
from osgeo import gdal
from osgeo import gdal_array
from osgeo import osr
import geopandas as gpd

## Maths libraries
import math

## Programming libraries
import numpy

print(sys.version)
print('flopy version: {}'.format(flopy.__version__))


# Load our old friend...the Freyberg model

# In[2]:


nam_file = "SPRING.nam"
model_ws = os.path.join("/","work", "04950", "dhl", "wrangler", "vc", "mod2-aid", "TWDB-GAMs-2016", "ebfz_b_v1.01", "ebfz_b_CD-2_model", "modflow", "ebfz_b_mf_Tran-mf05-spring-man-cdf")
ml = flopy.modflow.Modflow.load(nam_file, model_ws=model_ws, check=False)


# We can see the ``SpatialReference`` instance has generic entries, as does ``start_datetime``

# In[3]:


#ml.dis.sr


# In[4]:


#ml.dis.start_datetime


## Load relevant shapefile

shp_file = os.path.join(model_ws, "grid", "ebfz_b_grid_poly082615.shp")
model_grid = gpd.read_file(shp_file)


############################
## Determination of rotation
##

model_grid_maxR = model_grid.ROW.max()
model_grid_maxC = model_grid.COL.max()

## Indices of interest
model_grid_i1 = model_grid_maxC-1
model_grid_i2 = model_grid_maxC*(model_grid_maxR-1)
model_grid_i3 = model_grid_maxC*model_grid_maxR-1

## 'float_order' here refers to the order of the smallest decimal number
##  in magnitude whose first digit is one which may be represented
##  by Python's type, float64
float_order = -(len(repr(2**(2**10-1)).rstrip('L'))-1)

## Rotation counterclockwise off of the positive horizontal
## As the two floating point numbers compared may be large or near zero,
##  we must account for the intricacies of floating point comparison
##  for arbitrary values. To do so, we provide the least value for 'rel_tol'
##  possible without losing meaning to the finite decimal representation
##  of a floating point number. To 'abs_tol' we assign
##  the least order of magnitude possible in order to catch
##  the not-too-unlikely comparison of zeroes
## For reference and quality discussion surrounding this need,
##  see the following, ordered by priority:
##  https://www.python.org/dev/peps/pep-0485
##  https://stackoverflow.com/questions/5595425/what-is-the-best-way-to-compare-floats-for-almost-equality-in-python
if math.isclose( model_grid.CentroidX[0], model_grid.CentroidX[model_grid_i1], rel_tol=10**-9, abs_tol=10**float_order ):
	rotation = 0.0
elif math.isclose( model_grid.CentroidY[0], model_grid.CentroidY[model_grid_i1], rel_tol=10**-9, abs_tol=10**float_order ):
	rotation = 0.0
else:
	rotation = math.atan( ( model_grid.CentroidY[model_grid_i1] - model_grid.CentroidY[0] ) / ( model_grid.CentroidX[model_grid_i1] - model_grid.CentroidX[0] ) )
	if rotation < 0.0:
		rotation = math.pi/2. - rotation
#		row_usual = False
#########################


## Derivation of upper-left corner from provided shapefile, cf:
##  https://gis.stackexchange.com/a/274341

if math.isclose( rotation, 0.0, rel_tol=10**-9, abs_tol=10**float_order ):
	if math.isclose( model_grid.CentroidX[0], model_grid.CentroidX[model_grid_i1], rel_tol=10**-9, abs_tol=10**float_order ):
		if model_grid.CentroidY[0] > model_grid.CentroidY[model_grid_i2]:
			if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i1]:
				ml.dis.sr.xul = model_grid.CentroidX[0]
				ml.dis.sr.yul = model_grid.CentroidY[0]
				opp_xul = model_grid.CentroidX[model_grid_i3]
				opp_yul = model_grid.CentroidY[model_grid_i3]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i1]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i1]
				opp_xul = model_grid.CentroidX[model_grid_i2]
				opp_yul = model_grid.CentroidY[model_grid_i2]
		else:
			if model_grid.CentroidX[model_grid_i2] < model_grid.CentroidX[model_grid_i3]:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i2]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i2]
				opp_xul = model_grid.CentroidX[model_grid_i1]
				opp_yul = model_grid.CentroidY[model_grid_i1]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i3]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i3]
				opp_xul = model_grid.CentroidX[0]
				opp_yul = model_grid.CentroidY[0]
	else:
		if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i2]:
			if model_grid.CentroidY[0] > model_grid.CentroidY[model_grid_i1]:
				ml.dis.sr.xul = model_grid.CentroidX[0]
				ml.dis.sr.yul = model_grid.CentroidY[0]
				opp_xul = model_grid.CentroidX[model_grid_i3]
				opp_yul = model_grid.CentroidY[model_grid_i3]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i1]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i1]
				opp_xul = model_grid.CentroidX[model_grid_i2]
				opp_yul = model_grid.CentroidY[model_grid_i2]
		else:
			if model_grid.CentroidY[model_grid_i2] > model_grid.CentroidY[model_grid_i3]:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i2]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i2]
				opp_xul = model_grid.CentroidX[model_grid_i1]
				opp_yul = model_grid.CentroidY[model_grid_i1]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i3]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i3]
				opp_xul = model_grid.CentroidX[0]
				opp_yul = model_grid.CentroidY[0]
else: 
	if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i1]:
		if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i2]:
			ml.dis.sr.xul = model_grid.CentroidX[0]
			ml.dis.sr.yul = model_grid.CentroidY[0]
			opp_xul = model_grid.CentroidX[model_grid_i3]
			opp_yul = model_grid.CentroidY[model_grid_i3]
		else:
			ml.dis.sr.xul = model_grid.CentroidX[model_grid_i2]
			ml.dis.sr.yul = model_grid.CentroidY[model_grid_i2]
			opp_xul = model_grid.CentroidX[model_grid_i1]
			opp_yul = model_grid.CentroidY[model_grid_i1]
	else:
		if model_grid.CentroidX[model_grid_i1] < model_grid.CentroidX[model_grid_i3]:
			ml.dis.sr.xul = model_grid.CentroidX[model_grid_i1]
			ml.dis.sr.yul = model_grid.CentroidY[model_grid_i1]
			opp_xul = model_grid.CentroidX[model_grid_i2]
			opp_yul = model_grid.CentroidY[model_grid_i2]
		else:
			ml.dis.sr.xul = model_grid.CentroidX[model_grid_i3]
			ml.dis.sr.yul = model_grid.CentroidY[model_grid_i3]
			opp_xul = model_grid.CentroidX[0]
			opp_yul = model_grid.CentroidY[0]

print(ml.dis.sr.xul,ml.dis.sr.yul)


## Construction of needed Proj4 string from a .prj file provided by TWDB
## Drawn from Capooti of SE:
##  https://gis.stackexchange.com/a/7615

prj_file = open(os.path.join(model_ws,'grid/ebfz_b_grid_poly082615.prj'), 'r')
prj_txt = prj_file.read()
srs = osr.SpatialReference()
srs.ImportFromESRI([prj_txt])
proj4_str = srs.ExportToProj4()


## Ensure that output to NetCDF or GeoTIFF is in meters:
##
## To do so,, we simply check LENUNI and decide
ml.sr.units = 'meters'
if ml.sr.lenuni == 0:
	print('Length unit (LENUNI) undefined', file=sys.stderr)
	sys.exit()
elif ml.sr.lenuni == 1:
	ml.sr.length_multiplier = 0.3048
elif ml.sr.lenuni == 2:
	ml.sr.length_multiplier = 1.0
elif ml.sr.units == 3:
	ml.sr.length_multiplier = 1000.0


# Setting the attributes of the ``ml.dis.sr`` is easy:

# In[5]:


#ml.dis.sr.xul = 123457.7
#ml.dis.sr.yul = 765432.1
#rotation = 15.0
#proj4_str = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
#ml.dis.start_datetime = '7/4/1776'
ml.dis.start_datetime = '01/01/1989'
#ml.dis.sr


# In[6]:


#ml.dis.start_datetime


## Arrays to export to GeoTIFF:
##    - Storage
res_x = abs(ml.dis.sr.xul-opp_xul)/float(model_grid_maxC)
res_y = abs(ml.dis.sr.yul-opp_yul)/float(model_grid_maxR)
cbb = flopy.utils.binaryfile.CellBudgetFile(os.path.join(model_ws,'BUDGET.DAT'))
storage = cbb.get_data(text='STORAGE')#, full3D=True)
## 2018.11.01
## The following method to derive the rotation in each direction
##  is questionable. It has been left to be determined
##  either after Emily Pease sends her version of it
##  or the mistake becomes obvious after the creation of a GeoTIFF.
geotransform=(ml.dis.sr.xul,res_x,math.cos(rotation)*res_x,ml.dis.sr.yul,math.sin(rotation)*res_y,-res_y)
output_raster = gdal.GetDriverByName('GTiff').Create('test.tif',ncols,nrows,1,gdal.GDT_Float32)
output_raster.SetGeoTransform(geotransform)
srs = osr.SpatialReference()
srs.ImportFromEPSG(
## DEBUGGING
#print(geotransform)
#storage = numpy.array(storage,dtype=numpy.float64)
## Debugging lack of attribute, dtype, for this array
## SOLUTION: commented out immediately above
#for o in storage:
	#print(o)


# ### Some netCDF export capabilities:
# 
# #### Export the whole model (inputs and outputs)

# In[7]:


# make directory
pth = os.path.join(model_ws, 'exports')
if not os.path.exists(pth):
    os.makedirs(pth)


# In[8]:


#fnc = ml.export(os.path.join(pth, ml.name+'.in.nc'))
#hds = flopy.utils.HeadFile(os.path.join(model_ws,"heads.dat"))
#hds_file = os.path.join(model_ws,"HEADS.DAT")
#hds = flopy.utils.binaryfile.HeadFile(hds_file,precision='double')
#flopy.export.utils.output_helper(os.path.join(pth, ml.name+'.out.nc'), ml, {"hds":hds})


# #### export a single array to netcdf or shapefile

# In[9]:


# export a 2d array
#ml.dis.top.export(os.path.join(pth, 'top.nc'))
#ml.dis.top.export(os.path.join(pth, 'top.shp'))


# #### sparse export of stress period data for a boundary condition package  
# * excludes cells that aren't in the package (aren't in `package.stress_period_data`)  
# * by default, stress periods with duplicate parameter values (e.g., stage, conductance, etc.) are omitted
# (`squeeze=True`); only stress periods with different values are exported  
# * argue `squeeze=False` to export all stress periods

# In[10]:


#ml.drn.stress_period_data.export(os.path.join(pth, 'drn.shp'), sparse=True)


# #### Export a 3d array

# In[11]:


#export a 3d array
#ml.upw.hk.export(os.path.join(pth, 'hk.nc'))
#ml.upw.hk.export(os.path.join(pth, 'hk.shp'))


# #### Export a number of things to the same netCDF file

# In[12]:


# export lots of things to the same nc file
#fnc = ml.dis.botm.export(os.path.join(pth, 'test.nc'))
#ml.upw.hk.export(fnc)
#ml.dis.top.export(fnc)

# export transient 2d
#ml.rch.rech.export(fnc)


# ### Export whole packages to a netCDF file

# In[13]:


# export mflist
#fnc = ml.wel.export(os.path.join(pth, 'packages.nc'))
#ml.upw.export(fnc)
#fnc.nc


# ### Export the whole model to a netCDF

# In[14]:


#fnc = ml.export(os.path.join(pth, 'model.nc'))
#fnc.nc


## Export to GeoTIFF
##  including obligatory check as to existence of file
## !!!
## often encounter failure before this step due to a limitation in FloPy,
##  that is currently does not support non-uniform grids; there exist
##  several options:
##    - implement changes from:
##       https://github.com/modflowpy/flopy/pull/367/files
##    - recreate FloPy's implementation addressing issues as they arise:
##       https://github.com/modflowpy/flopy/blob/3f434b3cd53db53c66440b0e4b5668d0604481c6/flopy/utils/reference.py#L1094
## Export to shapefile
##  In order to include support for multiple attributes, FloPy's code appears
##   to indicate that the developers intend for users to use
##   flopy.export.shapefile_utils.write_grid_shapefile2
##   directly and rewrite portions of
##   flopy..utils.reference.SpatialReference.export_array
##   as needed.
#file_shape = os.path.join(pth,''.join([os.path.splitext(nam_file)[0],'.shp']))
#flopy.export.shapefile_utils.write_grid_shapefile2(filename=file_shape,sr=ml,array_dict={'storage': storage},nan_val=-9999,epsg=None,prj=proj4_str)
#print(storage.dtype)
#ml.sr.export_array(filename=os.path.join(pth,''.join([os.path.splitext(nam_file)[0],'-storage','.tif'])),a=storage[0])
