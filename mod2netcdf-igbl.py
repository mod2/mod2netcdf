
# coding: utf-8

# # FloPy
# 
# ### Demo of netCDF and shapefile export capabilities within the flopy export module. 

# In[1]:


## Libraries of context
from __future__ import print_function
import os
import sys

# run installed version of flopy or add local path
try:
    import flopy
except:
    fpth = os.path.abspath(os.path.join('..', '..'))
    sys.path.append(fpth)
    import flopy

## Geospatial libraries
from osgeo import osr
import geopandas as gpd

## Maths libraries
import math

print(sys.version)
print('flopy version: {}'.format(flopy.__version__))


# Load our old friend...the Freyberg model

# In[2]:


nam_file = "IGBL-CAL.nam"
model_ws = os.path.join("/","work", "04950", "dhl", "wrangler", "vc", "mod2-aid", "GAM_Distribution", "igbl_v1.01", "CD-2_model", "modflow", "Input", "trans", "calibration", "Calibration-mf05-igbl-cal-man-cdf")
ml = flopy.modflow.Modflow.load(nam_file, model_ws=model_ws, check=False)


# We can see the ``SpatialReference`` instance has generic entries, as does ``start_datetime``

# In[3]:


ml.dis.sr


# In[4]:


ml.dis.start_datetime


## Load relevant shapefile

shp_file = os.path.join(model_ws, "grid", "igbl_grid_poly082615.shp")
model_grid = gpd.read_file(shp_file)


############################
## Determination of rotation

model_grid_maxR = model_grid.ROW.max()
model_grid_maxC = model_grid.COL.max()

## Indices of interest
model_grid_i1 = model_grid_maxC-1
model_grid_i2 = model_grid_maxC*(model_grid_maxR-1)
model_grid_i3 = model_grid_maxC*model_grid_maxR-1

## 'float_order' here refers to the order of the smallest decimal number
##  in magnitude whose first digit is one which may be represented
##  by Python's type, float64
float_order = -(len(repr(2**(2**10-1)).rstrip('L'))-1)

## As the two floating point numbers compared may be large or near zero,
##  we must account for the intricacies of floating point comparison
##  for arbitrary values. To do so, we provide the least value for 'rel_tol'
##  possible without losing meaning to the finite decimal representation
##  of a floating point number. To 'abs_tol' we assign
##  the least order of magnitude possible in order to catch
##  the not-too-unlikely comparison of zeroes
## For reference and quality discussion surrounding this need,
##  see the following, ordered by priority:
##  https://www.python.org/dev/peps/pep-0485
##  https://stackoverflow.com/questions/5595425/what-is-the-best-way-to-compare-floats-for-almost-equality-in-python
if math.isclose( model_grid.CentroidX[0], model_grid.CentroidX[model_grid_i1], rel_tol=10**-9, abs_tol=10**float_order ):
	rotation = 0.0
elif math.isclose( model_grid.CentroidY[0], model_grid.CentroidY[model_grid_i1], rel_tol=10**-9, abs_tol=10**float_order ):
	rotation = 0.0
else:
	rotation = math.atan( ( model_grid.CentroidY[model_grid_i1] + model_grid.CentroidY[0] ) / ( model_grid.CentroidX[model_grid_i1] - model_grid.CentroidX[0] ) ) * 180 / math.pi
	if rotation < 0.0:
		rotation = 90.0 - rotation
#		row_usual = False
#########################


## Derivation of upper-left corner from provided shapefile, cf:
##  https://gis.stackexchange.com/a/274341

if math.isclose( rotation, 0.0, rel_tol=10**-9, abs_tol=10**float_order ):
	if math.isclose( model_grid.CentroidX[0], model_grid.CentroidX[model_grid_i1], rel_tol=10**-9, abs_tol=10**float_order ):
		if model_grid.CentroidY[0] > model_grid.CentroidY[model_grid_i2]:
			if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i1]:
				ml.dis.sr.xul = model_grid.CentroidX[0]
				ml.dis.sr.yul = model_grid.CentroidY[0]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i1]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i1]
		else:
			if model_grid.CentroidX[model_grid_i2] < model_grid.CentroidX[model_grid_i3]:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i2]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i2]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i3]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i3]
	else:
		if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i2]:
			if model_grid.CentroidY[0] > model_grid.CentroidY[model_grid_i1]:
				ml.dis.sr.xul = model_grid.CentroidX[0]
				ml.dis.sr.yul = model_grid.CentroidY[0]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i1]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i1]
		else:
			if model_grid.CentroidY[model_grid_i2] > model_grid.CentroidY[model_grid_i3]:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i2]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i2]
			else:
				ml.dis.sr.xul = model_grid.CentroidX[model_grid_i3]
				ml.dis.sr.yul = model_grid.CentroidY[model_grid_i3]
else: 
	if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i1]:
		if model_grid.CentroidX[0] < model_grid.CentroidX[model_grid_i2]:
			ml.dis.sr.xul = model_grid.CentroidX[0]
			ml.dis.sr.yul = model_grid.CentroidY[0]
		else:
			ml.dis.sr.xul = model_grid.CentroidX[model_grid_i2]
			ml.dis.sr.yul = model_grid.CentroidY[model_grid_i2]
	else:
		if model_grid.CentroidX[model_grid_i1] < model_grid.CentroidX[model_grid_i3]:
			ml.dis.sr.xul = model_grid.CentroidX[model_grid_i1]
			ml.dis.sr.yul = model_grid.CentroidY[model_grid_i1]
		else:
			ml.dis.sr.xul = model_grid.CentroidX[model_grid_i3]
			ml.dis.sr.yul = model_grid.CentroidY[model_grid_i3]


## Construction of needed Proj4 string from a .prj file provided by TWDB
## Drawn from Capooti of SE:
##  https://gis.stackexchange.com/a/7615

prj_file = open(os.path.join(model_ws,'grid/igbl_grid_poly082615.prj'), 'r')
prj_txt = prj_file.read()
srs = osr.SpatialReference()
srs.ImportFromESRI([prj_txt])
proj4_str = srs.ExportToProj4()


# Setting the attributes of the ``ml.dis.sr`` is easy:

# In[5]:


#ml.dis.sr.xul = 123457.7
#ml.dis.sr.yul = 765432.1
#rotation = 15.0
#proj4_str = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"
#ml.dis.start_datetime = '7/4/1776'
ml.dis.start_datetime = '01/01/1989'
ml.dis.sr


# In[6]:


ml.dis.start_datetime


# ### Some netCDF export capabilities:
# 
# #### Export the whole model (inputs and outputs)

# In[7]:


# make directory
pth = os.path.join(model_ws, 'data', 'netCDF_export')
if not os.path.exists(pth):
    os.makedirs(pth)


# In[8]:


#fnc = ml.export(os.path.join(pth, ml.name+'.in.nc'))
#hds = flopy.utils.HeadFile(os.path.join(model_ws,"heads.dat"))
#hds_file = os.path.join(model_ws,"HEADS.DAT")
#hds = flopy.utils.binaryfile.HeadFile(hds_file,precision='double')
#print(ml.name)
#flopy.export.utils.output_helper(os.path.join(pth, ml.name+'.out.nc'), ml, {"hds":hds})


# #### export a single array to netcdf or shapefile

# In[9]:


# export a 2d array
#ml.dis.top.export(os.path.join(pth, 'top.nc'))
#ml.dis.top.export(os.path.join(pth, 'top.shp'))


# #### sparse export of stress period data for a boundary condition package  
# * excludes cells that aren't in the package (aren't in `package.stress_period_data`)  
# * by default, stress periods with duplicate parameter values (e.g., stage, conductance, etc.) are omitted
# (`squeeze=True`); only stress periods with different values are exported  
# * argue `squeeze=False` to export all stress periods

# In[10]:


#ml.drn.stress_period_data.export(os.path.join(pth, 'drn.shp'), sparse=True)


# #### Export a 3d array

# In[11]:


#export a 3d array
#ml.upw.hk.export(os.path.join(pth, 'hk.nc'))
#ml.upw.hk.export(os.path.join(pth, 'hk.shp'))


# #### Export a number of things to the same netCDF file

# In[12]:


# export lots of things to the same nc file
#fnc = ml.dis.botm.export(os.path.join(pth, 'test.nc'))
#ml.upw.hk.export(fnc)
#ml.dis.top.export(fnc)

# export transient 2d
#ml.rch.rech.export(fnc)


# ### Export whole packages to a netCDF file

# In[13]:


# export mflist
#fnc = ml.wel.export(os.path.join(pth, 'packages.nc'))
#ml.upw.export(fnc)
#fnc.nc


# ### Export the whole model to a netCDF

# In[14]:


fnc = ml.export(os.path.join(pth, 'model.nc'))
fnc.nc

